# -*- coding: utf-8 -*-
"""
Created on Sun Oct  7 09:26:14 2018

@author: billy
"""

#!/usr/bin/env python


from pyclustering import utils
from pyclustering.utils import draw_clusters
from pyclustering.cluster import kmedoids
# load list of points for cluster analysis
sample = utils.read_sample('changedFomat.txt')
#sample = pan.read_csv('testing Data.csv')
# set random initial medoids
initial_medoids = [1, 10]
# create instance of K-Medoids algorithm
kmedoids_instance = kmedoids.kmedoids(sample,initial_medoids)
#.kmedoids(sample, initial_medoids)
# run cluster analysis and obtain results
kmedoids_instance.process();
clusters = kmedoids_instance.get_clusters()
# show allocated clusters
print(clusters)


print("done")