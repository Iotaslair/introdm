import java.util.ArrayList;

public class Intraspread {
	//Data =
	//Centers = ArrayList<ArrayList<Double>> where each item is a new column WATCH OUT FOR PEOPLE ADDING IN THE WRONG ORDER
	//Clusters = ArrayList<ArrayList<Double>> each inner arraylist is a point and the outer arraylist is the set of points
	public ArrayList<ArrayList<Double>> data = new ArrayList<ArrayList<Double>>();
	public ArrayList<ArrayList<Double>> centers = new ArrayList<ArrayList<Double>>();
	public ArrayList<ArrayList<Double>> clusters = new ArrayList<ArrayList<Double>>();
	
	public Intraspread(ArrayList<ArrayList<Double>> Data, ArrayList<ArrayList<Double>> Centers, ArrayList<ArrayList<Double>> Clusters)
	{
		data = Data;
		centers = Centers;
		clusters = Clusters;
	}
	
	public double calculate()
	{
		double sum = 0;
		//figure out a way to make sure the right points are being measured
		//make a distance function
		//or make an interface that classes have to follow and make the classes that follow it give a distance function
		for (ArrayList<Double> c : centers)
		{
			for(ArrayList<Double> p : clusters)
			{
				sum += distance(c,p);
			}
		}
		return sum;
			
		/*
		sum = 0;
		foreach c in clusters:
			foreach p in clusters[c]:
				sum += distance(c,p);
		return sum;
		*/
	}
}
