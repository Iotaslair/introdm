Instructions for runnning HCLUST:
	Ensure that the file you want to run it on is in the same directory and open the code in a python IDE and change the filename string to be the name of your file (by default it is BTCPreprocessedNoDates.csv.) Then, just simply run it on the command line or in your IDE's shell and it will output a Dendrogram
-Erik Ridd

Instructions for running K-Means:


Instructions for running K-Medoids:
	Open the code in a python IDE and make sure the data you're using is titled "BTCReformat.csv" and run the code
	It will output the clusters according to this protocol 
	[ 
	[ [center point1], [point 1 in this cluster], [point 2 in this cluster] etc.]
    [ [center point2], [point 1 in this cluster], [point 2 in this cluster] etc.]
	]

	I couldn't get inter and intra spread working so if you want to change the value of k it's at the bottom of the file at line 535
	It looks like this
	temp = KMedoids(df,3)

	Also it won't make a plot because I couldn't figure out how to get the protocol taht clusters follows to work with the built in PCA function with scikit learn