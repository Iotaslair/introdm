import java.util.ArrayList;

public class Interspread {
	ArrayList<ArrayList<Double>> centers = new ArrayList<ArrayList<Double>>();
	public Interspread(ArrayList<ArrayList<Double>> Centers)
	{
		centers = Centers;
	}
	
	public double calculate()
	{
		//figure out why distance needs to be squared
		//figure out how to get them to give me mean and distance (check interfaces)
		double center = mean(centers);
		double sum = 0.0;
		for( ArrayList<Double> c : centers)
		{
			sum += Math.pow(distance(c,center), 2);
		}
		return sum;
		
		/*
		center = mean(centers);
		sum = 0;
		foreach c in centers:
			sum += distance(c,center)^2
		return sum;
		*/
	}
}
