#A template for the implementation of hclust
import math #sqrt
import numpy as np
import pandas as pan
from scipy.cluster.hierarchy import dendrogram, linkage
from matplotlib import pyplot as plt



class Branch:
#This can be used to save the previous two points (roots)
#with the new merged one (head)
    def __init__(self, head, root1, root2):
        self.head = head
        self.root1 = root1
        self.root2 = root2
    def get(self):
        return self.head
    def getRoot1(self):
        return self.root1
    def getRoot2(self):
        return self.root2
    def getSelf(self):
        return self
    def __repr__(self):
        return np.array_str(self.head)




# import file and turn it into useable data
# We will be using the row and column method from Pandas
def Read(filename):
    df = pan.read_csv(filename)
    df = df.fillna(value=0)
    #print(df)
    return df;

def dfToList(df):
    rows = len(df.index)
    counter = 0
    da_list = []
    while(counter < rows):
        da_list.append(df.loc[counter].values)
        counter=counter+1
    return da_list


# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
#For now, I will use Euclidian Distance
# To pull a row from a df: print(df.loc[i].values)
# to pull a value from a row: print(df.loc[i].values[j])
def Distance(a,b):
    #a and b are seperate points, will have to account for this
    #I will assume the points are rows in the df
    #Step 1: Find number of columns
    columns = a.size
    assert(columns == b.size) #they should be ==
    
    #So, to do multidimensional ED, add the differences squared
    # between all the features and square root that.
    toSquare = 0 #We will add the distances to here
    counter = 0 #count through columns
    
    while counter < columns:
        toAdd = b.item(counter)-a.item(counter)
        toAdd=toAdd*toAdd
        toSquare = toSquare+toAdd
        counter = counter+1
    dist = math.sqrt(toSquare)
    #print(dist)
    return dist


# Accepts two data points a and b.
# Produces a point that is the average of a and b.
def merge(a,b):
    columns = a.size
    assert(columns == b.size) #they should be ==
    counter = 0
    newValues = []

    while counter < columns:
        avg = a.item(counter)+b.item(counter)
        avg = avg/2
        #print(avg)
        newValues.append(avg)
        counter=counter+1
    newPoint = np.array( newValues )
    #print("newPoint is ")
    #print(newPoint)
    return newPoint


# Accepts a list of data points.
# Returns the pair of points that are closest
def findClosestPair(D):
    #we will need to keep track of point 1 and point 2
    Dcopy = D.copy()
    p1 = None;
    p2 = None;
    dist = 2500000000000000000000000000;
    index = 0;
    index2 = 0;
    tempIndex=0;
    tempList = Dcopy.copy()
    if(len(tempList) == 1):
            item = tempList.pop(0)
            pair = [item, None, 0, None]
            return pair
    for item in Dcopy:
        
        
        tempIndex2=tempIndex
        tempList.pop(0)
        #print()
        #print(tempIndex)
        #print(len(tempList))
        for thing in tempList:
            #print(tempIndex2)
            tempDist = Distance(item, thing)
            if((tempDist < dist) and not np.array_equal(item, thing)):
              p1 = item
              #print(p1)
              p2 = thing
              #print(p2)
              #print()
              dist = tempDist
              index=tempIndex
              index2=tempIndex2
            tempIndex2=tempIndex2+1
        
        tempIndex=tempIndex+1
    pair = [p1,p2,index,index2]
    #print("pair = ")
    #print(pair)
    return pair


# Accepts a list o data points.
# Produces a tree structure corresponding to a 
# Agglomerative Hierarchal clustering of D.
def HClust(D):
    initialD =  D.copy()
    allPoints = []
    Dbranches = []
    nextD = []
    Zlist = []
    inZ = []

    counter = 0
    while(counter < len(initialD)):
        allPoints.append([initialD[counter], 1, 1])
        assert(np.array_equal(allPoints[counter][0], initialD[counter]))
        counter = counter+1
    print("here")
    

    while(len(initialD) > 0):
        tempPair = findClosestPair(initialD)#this is a list of two arrays
        count = 0
        noP2 = False
        p1 = tempPair[0]
        p2 = tempPair[1]
        if(np.array_equal(p2, None)):
            noP2 = True
        index1=tempPair[2]
        index2=tempPair[3]
        #print(tempPair[2])
        #print(tempPair[3])
        temp1 = initialD.pop(index1)
        assert(np.array_equal(temp1, p1))
        #print("One = ")
        #print(temp1)
        #print(p1)
        if(noP2==False):  
            temp2 = initialD.pop(index2)                     
            #print("Two = ")
            #print(temp2)
            #print(p2)
            assert(np.array_equal(temp2, p2))
            
            p1Branch = Branch(p1, None, None)
            p2Branch = Branch(p2, None, None)
            Dbranches.append(p1Branch)
            Dbranches.append(p2Branch)
            newP = merge(p1,p2)
            da_branch = Branch(newP,p1Branch,p2Branch)
            counter = 0
            true = False
            while(true == False):
                if(np.array_equal(da_branch.getRoot1().get(), allPoints[counter][0])):
                   true=True
                counter=counter+1
        if(noP2==True):
            da_branch = Branch(p1, None, None)
        nextD.append(da_branch)
        Dbranches.append(da_branch)
        print("alive")
    #print("HCLust:")

    #print(len(allPoints))
    #print(len(nextD))
    print("made it here!")
    for item in nextD:
        if(np.array_equal(item.getRoot1(), None)):
            zero=0
        else:
            counter = 0
            stop = False
            indx1 = None
            indx2 = None
            while(stop == False):
                #print(counter)
                #print(indx1)
                #print(indx2)
                if(np.array_equal(item.getRoot1().get(), allPoints[counter][0])):
                    indx1 = counter
                if(np.array_equal(item.getRoot2().get(), allPoints[counter][0])):
                    indx2 = counter
                if(indx1 != None):
                    if(indx2 != None):
                        stop = True
                counter = counter+1
            numOg = allPoints[indx1][2] + allPoints[indx2][2]
            #print(numOg)
            dis = Distance(item.getRoot1().get(), item.getRoot2().get())
            Zlist.append([indx1, indx2, dis, numOg])
            inZ.append(indx1)
            inZ.append(indx2)
            print("alive")
            allPoints.append([item.get(), len(allPoints), numOg])
    #print(Zlist)
    
    #assert(0==1)
    while(len(nextD)>1):
        nextD = BranchHClust(nextD)
        #print("new NextD")
        #print(len(allPoints))
        #print(len(nextD))
        for item in nextD:
            if(np.array_equal(item.getRoot1(), None)):
                zero=0
            else:
                counter = 0
                stop = False
                indx1 = None
                indx2 = None
                while(stop == False):
                    #print(counter)
                    #print(indx1)
                    #print(indx2)
                    if(np.array_equal(item.getRoot1().get(), allPoints[counter][0])):
                        indx1 = counter
                    if(np.array_equal(item.getRoot2().get(), allPoints[counter][0])):
                        indx2 = counter
                    if(indx1 != None):
                        if(indx2 != None):
                           stop = True
                           #print("here")
                    counter = counter+1
                
                if indx1 not in inZ:
                    inZ.append(indx1)
                    inZ.append(indx2)
                    numOg = allPoints[indx1][2] + allPoints[indx2][2]
                    #print(numOg)
                    dis = Distance(item.getRoot1().get(), item.getRoot2().get())
                    Zlist.append([indx1, indx2, dis, numOg])
                    allPoints.append([item.get(), len(allPoints), numOg])
                    print("added another Z")
                
        #print(Zlist)
    #print("hclust")
    return Zlist


#Used to recursively handle the steps after we get through the first part of HClust
def BranchHClust(D):
    #returns a Branch that is the last one, with references pointing to the rest of the tree

    #Make a list of the new Points
    branches = D.copy()
    initialD = []
    nextD = []
    print("still alive")
    for item in D:
        
        initialD.append(item.get())
    while(len(initialD) > 0):
            tempPair = findClosestPair(initialD)#this is a list of two arrays
            count = 0
            noP2 = False
            p1 = tempPair[0]
            p2 = tempPair[1]
            if(np.array_equal(p2, None)):
                noP2 = True
            index1=tempPair[2]
            index2=tempPair[3]
            #print(tempPair[2])
            #print(tempPair[3])
            temp1 = initialD.pop(index1)
            tempBranch1 = branches.pop(index1)
            assert(np.array_equal(temp1, p1))
            assert(np.array_equal(temp1,tempBranch1.get()))
            #print("One = ")
            #print(temp1)
            if(noP2==False):  
                temp2 = initialD.pop(index2)
                tempBranch2 =branches.pop(index2)
                #print("Two = ")
                #print(temp2)
                #print(p2)
                assert(np.array_equal(temp2, p2))
                assert(np.array_equal(temp2,tempBranch2.get()))
                newP = merge(p1,p2)
                da_list = [newP, tempBranch1, tempBranch2]
                da_branch = Branch(newP,tempBranch1,tempBranch2)
            if(noP2==True):
                da_branch = tempBranch1
            nextD.append(da_branch)     
    
    return nextD





testdf = Read('BTCPreprocessedNoDate.csv')
#testdist = Distance(testdf.loc[1].values, testdf.loc[5].values)
#print(testdist)
#testMerge = merge(testdf.loc[1].values, testdf.loc[5].values)
dfList = dfToList(testdf)
#print("testing closest")
#testClosest = findClosestPair(dfList)
print("read in")
Z = HClust(dfList)

#dendogram
plt.figure(figsize=(25,10))
plt.title('BitCoin HClust Dendrogram')
plt.xlabel('Indexes')
plt.ylabel('distances')
dendrogram(Z,
           leaf_rotation=90.,
           leaf_font_size=8.,
           )
plt.show()
