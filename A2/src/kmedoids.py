

#A template for the implementation of K-Medoids.
import math #sqrt

import pandas as pan
import numpy as np
import random
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt


#When you see commented out code it means I attempted something there and it failed
#Basically ignore it
#I also commented out code that would do stuff that I couldn't do

# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
    
# =============================================================================
#     print("a" + str(a))
#     print("b" + str(b))
# =============================================================================
    runningTotal = 0
# =============================================================================
#     print()
# =============================================================================
    for i in range(1,12):
    #for i in range(0,2):
# =============================================================================
#         print(a[i])
#         print(b[i])
# =============================================================================
        mid = a[i]-b[i]
# =============================================================================
#         print("mid: " + str(mid))
# =============================================================================
        runningTotal = runningTotal + math.pow(mid,2)
    
    #print("distance: " + str(math.sqrt(runningTotal)))
    return math.sqrt(runningTotal)

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
# Note: This can be identical to the K-Means function of the same name.
    
# =============================================================================
# clusters follows this protocol 
# [ 
#   [ [center point], [point 1 in this cluster], [point 2 in this cluster] etc.]
#   [ [center point], [point 1 in this cluster], [point 2 in this cluster] etc.]
# ]
# =============================================================================


def assignClusters(D, centers):
    
# =============================================================================
#     print("Type of D")
#     print(type(D))
#     print("Type of centers")
#     print(type(centers))
#     print("D[0]")
#     print(D[0])
#     print("centers[0]")
#     print(centers[0])
#     arr = np.array(3)
# =============================================================================
    
    
    
    #fakeCenters is a copy of the centers which will eventually become our output
    
    
    
    
    fakeCenters = [centers[0]]
    for i in range(1,len(centers)):
        fakeCenters.append(centers[i])
# =============================================================================
#     print()
#     print("fakeCenters at start")
#     print(fakeCenters)
#     
#     print()
#     print("D")
#     print(D)
#     print(type(fakeCenters[0]))
# 
#     temp = fakeCenters.copy()
#     print("Type of Temp")
#     print(type(temp))
# =============================================================================

    #loop through all data points                    i
    for i in range(0,len(D)):
    #for i in range(0, 4):
# =============================================================================
#         print()
#         print("Point we care about")
#         print(D[i])
#         print()
#         #index of the center with the lowest distance
# =============================================================================
        lowestCenterYetIndex = 0
        #int lowest (distance between this point and center)
        lowest = 10**50
        #point lowestCenterYet (start with centers[0])
        lowestCenterYet = centers[0]
        #loop through centers                        j
        for j in range(0,(int) (len(centers))):
            #distance of data point i to center j            
            distance = Distance(D[i], centers[j])
# =============================================================================
#             print("Centers[j]")
#             print(centers[j])
#             print("Distance")
#             print(distance)
# =============================================================================
            
            #if distance(i,j) < lowest
            if(distance < lowest):
                #change lowest to distance(i,j)
                lowest = distance
# =============================================================================
#                 print("Lowest reassigned")
#                 print(lowest)
# =============================================================================
                #changed lowestCenterYet to center j
                lowestCenterYet = centers[j]
                lowestCenterYetIndex = j
        #assign point i to lowestCenterYet
        #print("finished finding closest center")
        #copy entire clusters
        temp = fakeCenters.copy()
        #clear out things we don't care about
        temp.clear()
        #add in list of center and other points we care about (lowestCenterYet)
        
# =============================================================================
#         print()
#         print("Going to add the point we care about to")
#         print(lowestCenterYet)
#         print(lowest)
#         print()
# =============================================================================
        #FIX IT SO THAT IT ADDS IN LOWESTCENTERYET
        
        
        
        #find the what value lowestCenterYet is
# =============================================================================
#         x = 0
#         stupid = False
#         for j in range(0, len(fakeCenters)):
#             print("At the start hopefully it didn't change")
#             print(fakeCenters)
#             
#             print()
#             print("Value of stupid")
#             print(stupid)
#             if stupid == True:
#                 j = j - 1
#                 break
#             print("fakeCenters[j]")
#             print(fakeCenters[j])
#             print("Type of fakeCenters[j]")
#             print(type(fakeCenters[j]))
#             print()
#             #if(len(fakeCenters[j]) == 2):
#             if isinstance(fakeCenters[j],np.ndarray):
#                 print()
#                 print(fakeCenters[j])
#                 print("len(fakeCenters[j])")
#                 print(len(fakeCenters[j]))
#                 for k in range(0,len(lowestCenterYet)):
#                     if(lowestCenterYet[k] == fakeCenters[j][k]):
#                         stupid = True
#                         print("I like this")
#                         print(fakeCenters[j])
#                         print("j")
#                         print(j)
#                     else:
#                         #print("I don't like this")
#                         stupid = False
#             else:
#                 for k in range(0,len(lowestCenterYet)):
#                     print("lowestCenterYet[k]")
#                     print(lowestCenterYet[k])
#                     print("fakeCenters[j][0][k]")
#                     print(fakeCenters[j][0][k])
#                     if(lowestCenterYet[k] == fakeCenters[j][0][k]):
#                         stupid = True
#                     else:
#                         stupid = False
#                         break
#         if stupid:
#             x = j
# =============================================================================
        x = lowestCenterYetIndex

        
# =============================================================================
#         np.append(fakeCenters,x, D[i], axis = 2)
# =============================================================================
        
# =============================================================================
#         print()
#         print("x")
#         print(x)
#         print("fakeCenters[x]")
#         print(fakeCenters[x])
#         print("type(fakeCenters[x])")
#         print(type(fakeCenters[x]))
#         print("len(fakeCenters[x])")
#         print(len(fakeCenters[x]))
# =============================================================================
        
        
        if(type(fakeCenters[x]) == list):
            for j in range(0,len(fakeCenters[x])):
                temp.append(fakeCenters[x][j])
        else:
            if(len(fakeCenters[x]) == 2):
                temp.append(fakeCenters[x])
                
                
# =============================================================================
#         print("Temp before appending our point")
#         print(temp)
# =============================================================================
        temp.append(D[i])
# =============================================================================
#         print("Temp after appending our point")
#         print(temp)
# =============================================================================
        #add in everything from fakeCenters to temp
# =============================================================================
#         for i in range(0,len(fakeCenters[j])):
#             temp.append(fakeCenters[j][i])
# =============================================================================
        
# =============================================================================
#         print("Temp before appending our point")
#         print(temp)
# =============================================================================
# =============================================================================
#         print("lowestCenterYet")
#         print(type(lowestCenterYet[0]))
#         print(lowestCenterYet[0])
# =============================================================================
        
        
        
# =============================================================================
#         
# # =============================================================================
# #         print("lowestCenterYet")
# #         print(type(lowestCenterYet[0]))
# #         print(lowestCenterYet[0])
# # =============================================================================
#         #print(fakeCenters)
#         ans = 0 
# 
#         for j in range(0,len(fakeCenters)):
#             if(type(fakeCenters[j][0]) == str):
#                 if(lowestCenterYet[0] == fakeCenters[j][0]):
#                     ans = j
#                     temp.append(fakeCenters[ans])
#                     break
#             else:
# # =============================================================================
# #                 print("lowestCenterYet")
# #                 print(lowestCenterYet[0])
# #                 print("FakeCenters[j]")
# #                 print(fakeCenters[j])
# #                 print("FakeCenters[j][0]")
# #                 print(fakeCenters[j][0])
# #                 print("fakeCenters[j][0][0]")
# #                 print(fakeCenters[j][0][0])
# #                 print()
# # =============================================================================
#                 
#                 
#                 if(lowestCenterYet[0] == fakeCenters[j][0][0]):
#                     ans = j
#                     for k in range(0,len(fakeCenters[ans])):
#                         temp.append(fakeCenters[ans][k])
#                     break
# =============================================================================

        
# =============================================================================
#         add point
#         temp.insert(1,D[i])
#         temp.append(D[i])
# =============================================================================
        
# =============================================================================
#         print("Temp after appending our point")
#         print(temp)
# =============================================================================
        
# =============================================================================
#         add in temp to the right spot
# =============================================================================
        fakeCenters[x] = temp
# =============================================================================
#         print("fakeCenters")
#         print(fakeCenters)
# =============================================================================
        
# =============================================================================
#         print()
#         print("FakeCenters after append")
#         print(fakeCenters)
#         print()
#         print("Going to next Point")
# =============================================================================
# =============================================================================
#         print("fakeCenters")
#         print(fakeCenters)
#         make lowest = 0
#         lowest = 0
#         make lowestCenterYet center[0]
#         lowestCenterYet = center[0]
# =============================================================================
            
# =============================================================================
#     print("fakeCenters right before exiting")
#     print(fakeCenters)
# =============================================================================
    return fakeCenters
    

# Accepts a list of data points.
# Returns the medoid of the points.
def findClusterMedoid(cluster):
    #Go through the list and find distance from this point to every other point
    #pick lowest number
    if(type(cluster) == np.ndarray):
# =============================================================================
#         print("it's a np array")
# =============================================================================
# =============================================================================
#         if(cluster.size == 1):
#             print("cluster of size 1")
# =============================================================================
        return cluster
    #lowest distance thus far
    lowestDistance = 10**50
# =============================================================================
#   print(cluster.size)
# =============================================================================
    #point with lowest distance
    lowestPoint = cluster[0]
    #loop through list of points                i
    for i in range(0, len(cluster)):
        thisDistance = 0
        #loop through list of points again  j
        for j in range(0, len(cluster)):
            #if points are equal ignore
            if(i == j):
                pass
            else:
                #distance from point i to point j
                #sum all these distances
# =============================================================================
#                 print("i: " + str(cluster[i]))
#                 print("j: " + str(cluster[j]))'
#                 print("cluster[i]")
#                 print(cluster[i])
#                 print("cluster[j]")
#                 print(cluster[j])
# =============================================================================
                thisDistance = thisDistance + Distance(cluster[i],cluster[j])
# =============================================================================
#                 print()
# =============================================================================
                
        #if distance for point i < lowest distance thus far
        if(thisDistance < lowestDistance):
# =============================================================================
#             print("Changing lowest")
# =============================================================================
            #store lowest distance as i 
            lowestDistance = thisDistance
            #store lowest point
            lowestPoint = cluster[i]
    
# =============================================================================
#     print("Lowest Point")
#     print(lowestPoint)
# =============================================================================
    return lowestPoint

    #determines if past centers == centers (determines if we're done with the loop in kmedoids)
def again(centers,pastCenters):
    loopAgain = False
    #loop through every point   i
# =============================================================================
#     print("len(centers)")
#     print(len(centers))
# =============================================================================
    for i in range(0,len(centers)):
        #loop through every entry in point i   j
# =============================================================================
#         print("len(centers[i])")
#         print(len(centers[i]))
# =============================================================================
        for j in range(0, len(centers[i])):
            if centers[i][j] == pastCenters[i][j]:
                loopAgain = False
            else:
                return True
    return loopAgain


    #Functions for picking k (I think they work I just couldn't pass in the right parameters)
# =============================================================================
# 
# def intraspread(centers, clusters):
#     sum = 0
#     for i in range(0,len(centers)):
#         for j in range(1,len(clusters[i])):
#             sum += Distance(centers[i],clusters[i][j])
#     return sum
# 
# def interspread(centers):
#     center = makePoint(centers)
#     print("Center point")
#     print(center)
#     sum = 0
#     for i in range(0,len(centers)):
#         sum += Distance(centers[i],center)
#     return sum
# 
# 
# def makePoint(centers):
#     #temp = new list i think
#     temp = centers[0].copy()
#     for i in range(0,len(temp)):
#         temp[i]  = 0
#     #print(temp)
#     #loop through every center  i
#     for i in range(0,len(centers)):
#         for j in range(0,len(centers[i])):
#         #for j in range(1,len(centers[i])):
#             #sum = sum + centers[i][j]
#             try:
#                 temp[j] =  temp [j] + centers[i][j]
#             except:
#                 temp[j] = centers[i][j]
#     for i in range(0,len(temp)):
#         temp[i] = temp[i] / len(centers)
#     return temp
# =============================================================================

    
# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Medoids clustering
#  of D.
def KMedoids(D, k):
    row = D.get_values()
# =============================================================================
#     print(row)
#     print(row.size/12)
# =============================================================================
    centers = []
    temp = []
    for i in range(0,k):
        x = random.randint(0,len(row) - 1)#/12) -1
        while(x in temp):
            x = random.randint(0, len(row) - 1)#/12) - 1)
# =============================================================================
#         print(x)
# =============================================================================
        temp.append(x)
        centers.append(row[x])

    pastCenters = []
    #number of time we've been through the loop 
    counter = 0
    
    print("Counter is a variable that counts how many iterations we went through")
    
    loopAgain = True
    while(loopAgain and counter < 10):
        print("Counter")
        print(counter)
        counter = counter + 1
        pastCenters.clear()
        for i in range(0,len(centers)):
            pastCenters.append(centers[i])
# =============================================================================
#         pastCenters = centers
# =============================================================================
        clusters = assignClusters(row,centers)
        print("Done assigning clusters")
        print("Now finding the middle of those clusters")
# =============================================================================
#         print(clusters)
# =============================================================================
        centers.clear()
        for i in range(0,k):
            
# =============================================================================
#             print("i")
#             print(i)
#             print("cluster[i]")
#             print(clusters[i])
#             print()
# =============================================================================
            
            x = findClusterMedoid(clusters[i])
            centers.append(x)
        print("Centers currently")
        print(centers)
# =============================================================================
#         print("pastCenters")
#         print(pastCenters)
# =============================================================================
        loopAgain = again(centers,pastCenters)
        
    #code for interspread and intra spread to pass variables around 
# =============================================================================
#     print("Distance between centers")
#     print(interspread(centers))
#     
#     fakeClusters = clusters.copy()
#     for i in range(0,len(clusters)):
#         #loop through values
#         print("clusters")
#         print(clusters)
#         fakeClusters[i].clear()
#     
# # =============================================================================
# #      #loop through clusters
# #     for i in range(0,len(clusters)):
# #         #loop through values
# #         for j in range(0,len(clusters[i])):
# #             print("i")
# #             print(i)
# #             print("j")
# #             print(j)
# #             fakeClusters[i][j] = clusters[i][j]
# # =============================================================================
#     print("FakeClusters")
#     print(fakeClusters)
#     #loop through clusters
#     for i in range(0,len(clusters)):
#         #loop through values
#         print(i)
#         print(clusters[i])
#         fakeClusters[i] = clusters[i] [1:]
#         print(fakeClusters)
#     
#     
#     
#     print("FakeClusters end")
#     print(fakeClusters)
#     print("Distance within Clusters")
#     print(intraspread(centers,))
# =============================================================================
    return centers

df = pan.read_csv('BTCReformat.csv')
#df = pan.read_csv('testing Data.csv')

# =============================================================================
# print(df)
# =============================================================================
row = df.get_values()
# =============================================================================
# print("Start of row")
# =============================================================================

# =============================================================================
# print("a " + str(row[0]))
# print("End of row")
# temp = Distance(row[0],row[1])
# =============================================================================
temp = KMedoids(df,3)
print()
print("K-Medoids is done")
print("Centers")
print(temp)

        #Code for doing PCA turned out I couldn't around to this 
# =============================================================================
# def doPCA(temp):
#   print("Testing")
#   pca = PCA(n_components=2)
#   pca.fit(temp)
#   return pca
# 
# pca = doPCA(temp)
# print(pca.explained_variance_ratio_)
# first_pc = pca.components_[0]
# second_pc = pca.components_[1]
# 
# transformed_data = pca.transform(temp)
# 
# for ii, jj in zip(transformed_data,temp):
#     plt.scatter(first_pc[0] * ii[0], first_pc[1] * ii[0], color = "r")
#     plt.scatter(second_pc[0] * ii[1], second_pc[1] * ii[1], color = "c")
#     plt.scatter(jj[0], jj[1], color = "b")
#     
# 
# plt.xlabel("PC1")
# plt.ylabel("PC2")
# plt.show()
# =============================================================================
