import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

public class PreProcessor
{
    public static void main(String[] args){
        CSVProcessor csvp = new CSVProcessor();
        
        File toOpen = new File("Project_Data_Copy.csv");
        ArrayList<String[]> data = csvp.Read(toOpen);
        csvp.Write(data, "Project_Data_Copy_Random_Selection.csv", true);
        //remove redundncy
        ArrayList<Integer> cN = new ArrayList<Integer>();
        cN.add(0); cN.add(2); cN.add(4); cN.add(5);
        cN.add(6); cN.add(7); cN.add(8); cN.add(9); cN.add(10);
        cN.add(11); cN.add(12); cN.add(13); cN.add(14); cN.add(15); 
        ArrayList<String[]> data2 = csvp.removeColumns(data, cN);
        csvp.Write(data2,"Project_Data_With_Removed_Columns.csv", true);
        //split the number
        ArrayList<String[]> dataSplit = new ArrayList(data2);
        ArrayList<Integer> cNS = new ArrayList();
        cNS.add(0);
        dataSplit = csvp.splitColumns(dataSplit, cNS, false);
        csvp.Write(dataSplit,"Project_Data_Copy_final.csv", true);
        //select randomly
        //ArrayList<String[]> randomSelected = csvp.randomSelect(dataSplit, 200000);
        //csvp.Write(randomSelected,"Project_Data_Copy_final.csv", true);
        
        
        
        //start counting at 0
        /*
        //split columns
        ArrayList<String[]> dataSplit = new ArrayList(data);
        ArrayList<Integer> cNS = new ArrayList();
        cNS.add(1); cNS.add(2);
        dataSplit = csvp.splitColumns(dataSplit, cNS, true);
        csvp.Write(dataSplit,"a1_test_clone_split.csv", true);
        
        csvp.Write(data, "a1_test_clone.csv", true);
        //test remove4
        ArrayList<String[]> data2 = csvp.remove4Columns(data,6, 11, 12, 15);
        csvp.Write(data2, "a1_test_clone_Removed_Values.csv", true);
        
        //test remove columns
        ArrayList<Integer> cN = new ArrayList<Integer>();
        cN.add(6);
        cN.add(11);
        cN.add(12);
        cN.add(15);
        
        
        ArrayList<String[]> data3 = csvp.removeColumns(data, cN);
        csvp.Write(data3, "a1_test_clone_Better_Removed_Values.csv", true);
        */
        
    }
}
