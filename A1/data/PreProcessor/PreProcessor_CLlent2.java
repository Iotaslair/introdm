import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;


public class PreProcessor_CLlent2
{
    public static void main(String[] args2){
        CSVProcessor csvp = new CSVProcessor();
        
        File toOpen = new File("bakery - Copy.dat");
        ArrayList<String[]> data = csvp.ReadNonCsv(toOpen);
        csvp.Write(data,"bakery_clone.csv.", true);
        
        ArrayList<String[]> data2 = csvp.addLeadingZero(data);
        csvp.Write(data2, "bakery_with_leading_zeroes.csv", true);
        
        File items = new File("ItemListing.txt");
        ArrayList<String[]> itemList = csvp.ReadNonCsv(items);
        csvp.Write(itemList, "ItemListing.csv", true);
        ArrayList<String[]> data3 = csvp.replaceWithItem(data2, itemList);
        csvp.Write(data3, "bakery_as_items.csv", true);
        
        
}
}
