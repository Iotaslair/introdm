import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.*;


public class FinishProcessor
//processor to finish project_data_clone_final
{
    public FinishProcessor(){
    }
    
    public void Process(File file){
        String data = "";
        
        
        try{
            Scanner sc = new Scanner(file).useDelimiter(",|\\n");
            //add the header first
            data = data+"Pickup_Day,Pickup_Time,Passengers,Fare,\n";
            //move on to column loop
            int row = 1;
            while(sc.hasNext()){
                //get the pickup day first
                String dT = sc.next();
                //System.out.println(dT);
                String[] dayTemp = dT.split("/"); //remove the slashes
                data = data+dayTemp[1]+","; //we want the middle digit, the day
                //System.out.println("day = "+dayTemp[1]);
                
                //get the time
                String[] timeTemp = sc.next().split(":");
                int hourTemp = Integer.valueOf(timeTemp[0]); 
                String timeOfDay = "";
                //convert the hour into the time of day
                if(hourTemp < 6) timeOfDay = "Midnight";
                if(hourTemp >=6 && hourTemp < 12) timeOfDay = "Morning";
                if(hourTemp >=12 && hourTemp < 18) timeOfDay = "Afternoon";
                if(hourTemp >=18 && hourTemp <= 24) timeOfDay = "Evening";
                //System.out.println(hourTemp + " "+ timeOfDay);
                data = data+timeOfDay+",";
                
                //Passenger needs no changes
                int pass = sc.nextInt();
                //System.out.println(pass);
                data = data+pass+",";
                
                //convert total amount into category
                String mT = sc.next();
                //System.out.println(mT);
                int moneyTemp;
                try{
                    moneyTemp = Integer.valueOf(mT);
                }catch(NumberFormatException ex){
                    String[] mTemp = mT.split("\\.");
                    mTemp[0] = mTemp[0].replace("\n","");
                    moneyTemp = Integer.valueOf(mTemp[0]);
                }
                //String[] mTemp = mT.split("\\.");
                //System.out.println(mTemp[0]);
                //System.out.println(mTemp.length);
                //int moneyTemp = Integer.valueOf(mTemp[0]);
                //int moneyTemp = Integer.valueOf(mT);
                String fare = "";
                if(moneyTemp < 15) fare = "Small";
                if(moneyTemp >=15 && moneyTemp <25) fare = "Medium";
                if(moneyTemp >=25) fare = "Big";
                //System.out.println(fare);
                data = data+fare;
                
                //add the newline
                data = data+"\n";
                //System.out.println("row = "+row);
                row++;
            }
        }
        catch(FileNotFoundException e){
                e.printStackTrace();
        }
        System.out.println("Data has been read!");
        
        //write a new file
        FileWriter newFile = null;
        try{
            newFile = new FileWriter("Project_Data_Processed_Final.csv");
            newFile.append(data);
        }
        catch (Exception e){
            System.out.println("Error writing file!");
            e.printStackTrace();
        }
        finally{
            try{
                
                newFile.close();
                System.out.println("New file Written: Project_Data_Processed_Final.csv!");
            }
            catch (Exception e){System.out.println("Error closing filewriter!");
            e.printStackTrace();
            }
        }
    }
    
    
    public static void main(String[] args){
     FinishProcessor fp = new FinishProcessor();   
     File file = new File("Project_Data_Copy_final.csv");
     fp.Process(file);
        
    }
    
}
