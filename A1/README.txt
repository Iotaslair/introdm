Please provide instructions here for how to build and run your project.

The preferred build system is make.

If you do not know make, any other single command that will build and run your project is acceptable.



Our file was too large for git lfs to handle without paying so we uploaded our data to google drive. 

https://drive.google.com/drive/folders/1bW5nXzQNkF_v0UwLd1ghzaAlOX8rXYCE?usp=sharing

From here go to into the terminal/command line/command prompt 
Then navigate to wherever you downloaded the repository by typing in cd and the directory you want to go to next and clicking enter
Then go to /A1/src and click enter
From here type in "java Apriori" without the quotes and click enter
After that you need to tell the program where the data you downloaded earlier from the google drive is located at. 
Don't forget about .csv after you type in the directory and the slash at the start.
If you get it wrong then just reopen the java file by doing "java Apriori"
Once you get the data in correctly the program will run and produce the rules that fit the minimum thresholds of 
support >= .11
confidence >= .9
lift >= 3.2